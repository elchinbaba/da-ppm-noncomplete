class MatchingException(BaseException): pass

class CompleteMatchingException(MatchingException): pass

class NoncompleteMatchingException(MatchingException): pass