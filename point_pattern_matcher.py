from copy import deepcopy
from itertools import combinations

from helpers.helpers import CartesianPointSet, CartesianPoints, CartesianPoint, PolarPoint, is_equal
from helpers.types import MATCH
from helpers.constants import NORMALIZATION, SCALING, NORMALIZATION_ERROR, SCALING_ERROR
from logging_.log import log
from exceptions.exceptions import ScalingException, NormalizationException, CompleteMatchingException, NonexistentSynchronizationException, FindingException

class PointPatternMatcher:
    def __init__(self, A: CartesianPointSet, B: CartesianPointSet):
        self._A: CartesianPointSet
        self._B: CartesianPointSet
        self._accurately_placed: bool

        if A.len() <= B.len():
            self._A = A
            self._B = B

            self._accurately_placed = True
        else:
            self._A = B
            self._B = A

            self._accurately_placed = False

        self._synchronization_method: str = SCALING

    def match(self) -> MATCH:
        def match_complete(B: CartesianPointSet, A_r: float = None):
            def do_matching(matching: MATCH) -> MATCH:
                def match_remainings(matching: MATCH) -> MATCH:
                    if len(matching.keys()) == A.len():
                        return matching

                    for i in range(A.len()):
                        if (not i in matching.keys()):
                            try:
                                matching[i] = B.find(A[i].match(B.subtract([CartesianPoint(B[p].x, B[p].y) for p in matching.values()])))
                            except FindingException as ex:
                                log(*ex.args)

                                raise ex

                    return matching

                try:
                    B__: CartesianPointSet = deepcopy(B)

                    err: PolarPoint
                    if self._synchronization_method == NORMALIZATION:
                        err = NORMALIZATION_ERROR
                    elif self._synchronization_method == SCALING:
                        err = SCALING_ERROR
                    else:
                        exception_message: str = "Such a synchronization method does not exist: " + self._synchronization_method

                        raise NonexistentSynchronizationException(exception_message)

                    for i in range(A.len()):
                        a_i_polar: PolarPoint = A[i].convert_to_polar_point(centroid)

                        for j in range(B__.len()):
                            b_j_polar: PolarPoint = B__[j].convert_to_polar_point(centroid)

                            if is_equal(a_i_polar, b_j_polar, err):
                                try:
                                    matching[i] = B.find(B__[j])
                                except FindingException as ex:
                                    log(*ex.args)

                                    raise ex

                                del B__.points[j]

                                break

                    matching = match_remainings(matching)
                except:
                    exception_message: str = "Error happened during complete matching between " + str(A) + " and " + str(B_)

                    raise CompleteMatchingException(exception_message)

                return matching

            matching: MATCH = {}

            if self._synchronization_method == SCALING:
                try:
                    scale_factor: float = A_r / B.calc_smallst_enclos_circle().radius
                    B.scale(scale_factor)
                except ScalingException as ex:
                    log(*ex.args)

                    raise ex
            elif self._synchronization_method == NORMALIZATION:
                try:
                    B.normalize()
                except NormalizationException as ex:
                    log(*ex.args)

                    raise ex
            else:
                exception_message: str = "Such a synchronization method does not exist: " + self._synchronization_method

                raise NonexistentSynchronizationException(exception_message)

            centroid: CartesianPoint = A.calc_centroid()

            translation_offset: CartesianPoint = centroid - B.calc_centroid()
            B.transform(translation_offset)

            matching = do_matching(matching)

            return matching

        def check_matching(matching: MATCH, main_matching: MATCH, main_sum: float) -> tuple[MATCH, MATCH, float]:
            sum: float = 0
            for i in range(len(matching.keys())):
                sum += A[i].distance(B_[matching[i]])

            if sum < main_sum:
                main_sum = sum
                main_matching = matching

            return (matching, main_matching, main_sum)

        A: CartesianPointSet = deepcopy(self._A)
        B: CartesianPointSet = deepcopy(self._B)

        if self._synchronization_method == SCALING:
            try:
                A_r = A.calc_smallst_enclos_circle().radius
            except ScalingException as ex:
                log(*ex.args)

                self._synchronization_method = NORMALIZATION

                try:
                    A.normalize()
                except NormalizationException as ex:
                    log(*ex.args)

                    raise ex
        elif self._synchronization_method == NORMALIZATION:
            try:
                A.normalize()
            except NormalizationException as ex:
                log(*ex.args)

                raise ex
        else:
            exception_message = "Such a synchronization method does not exist: " + self._synchronization_method

            raise NonexistentSynchronizationException(exception_message)

        main_sum: float = 10**18
        main_matching: MATCH = {}

        B_indices: list[int] = [i for i in range(B.len())]
        for comb in combinations(B_indices, A.len()):
            B_: CartesianPointSet = CartesianPointSet(CartesianPoints([B[i] for i in comb]))

            matching: MATCH
            if self._synchronization_method == NORMALIZATION:
                try:
                    matching = match_complete(B_)
                except CompleteMatchingException as ex:
                    log(*ex.args)

                    raise ex
            elif self._synchronization_method == SCALING:
                try:
                    matching = match_complete(B_, A_r)
                except CompleteMatchingException as ex:
                    log(*ex.args)

                    raise ex
                except ScalingException as ex:
                    log(*ex.args)

                    self._synchronization_method = NORMALIZATION

                    return self.match()
            else:
                exception_message = "Such a synchronization method does not exist: " + self._synchronization_method

                raise NonexistentSynchronizationException(exception_message)

            matching, main_matching, main_sum = check_matching(matching, main_matching, main_sum)

        return main_matching if self._accurately_placed else dict((v, k) for k, v in main_matching.items())

def point_pattern_matcher(A: list[tuple], B: list[tuple]) -> MATCH:
    A_points: CartesianPointSet = CartesianPointSet(CartesianPoints([CartesianPoint(p[0], p[1]) for p in A]))
    B_points: CartesianPointSet = CartesianPointSet(CartesianPoints([CartesianPoint(p[0], p[1]) for p in B]))

    return PointPatternMatcher(A_points, B_points).match()
