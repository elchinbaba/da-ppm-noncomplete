from warnings import filterwarnings

from cv2 import minEnclosingCircle
from numpy import array

from helpers.point_set.points import CartesianPoints, PolarPoints, PolarPoint
from helpers.point_set.point import CartesianPoint
from helpers.point_set.circle import Circle

from helpers.min_enc_circ import minimum_enclosing_circle
from helpers.min_enc_circ_welzl import welzl

from exceptions.exceptions import ScalingException, NormalizationException, FindingException


class PointSet:
    pass

class CartesianPointSet(PointSet):
    def __init__(self, points: CartesianPoints) -> None:
        self.points = points

    def __getitem__(self, key) -> CartesianPoint:
        return self.points[key]

    def __str__(self) -> str:
        return str(self.points)

        return ", ".join([str(p) for p in self.points])

    def len(self) -> int:
        return len(self.points)

    def normalize(self) -> None:
        # normalization can also be done with respect to each coordinate element

        try:
            center = self.calc_centroid()
            self.scale(1 / max([center.distance(p) for p in self.points])) # sum
        except:
            exception_message = "Error happened while calculating the smallest enclosing circle of " + str(self)
            raise NormalizationException(exception_message)

    def scale(self, s: float) -> None:
        self.points = CartesianPoints([CartesianPoint(s * p.x, s * p.y) for p in self.points])

    def transform(self, t: CartesianPoint) -> None:
        self.points = CartesianPoints([CartesianPoint(p.x + t.x, p.y + t.y) for p in self.points])    

    def calc_centroid(self) -> CartesianPoint:
        return self.points.avg()

    def calc_smallst_enclos_circle(self):
        arr = array([(p.x, p.y) for p in self.points])

        filterwarnings("error")
        try:
            center, radius = minEnclosingCircle(arr)
        except BaseException:
            center, radius = welzl(arr)
            # center, radius = minimum_enclosing_circle(arr)
        except:
            exception_message = "Error happened while calculating the smallest enclosing circle of " + str(self)

            raise ScalingException(exception_message)

        return Circle(CartesianPoint(center[0], center[1]), radius)

    def subtract(self, point_set: CartesianPoints) -> 'CartesianPoints':
        return [p for p in self if not p.is_in(point_set)]

    def find(self, point: CartesianPoint) -> int:
        for i in range(len(self.points)):
            if self.points[i] == point:
                return i

        exception_message = "Point " + str(point) + " does not exist in " + str(self)
        raise FindingException(exception_message)

class PolarPointSet(PointSet):
    def __init__(self, points: PolarPoints) -> None:
        self.points = points

    def __getitem__(self, key) -> PolarPoint:
        return self.points[key]
