from helpers.point_set.point import CartesianPoint


class Circle:
    def __init__(self, p: CartesianPoint, r: float) -> None:
        self.centre = p
        self.radius = r

    def __str__(self):
        return str((str(self.centre), self.radius))
