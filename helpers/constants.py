from helpers.helpers import PolarPoint


NORMALIZATION: str = "normalization"
SCALING: str = "scaling"

NORMALIZATION_ERROR: PolarPoint = PolarPoint(0.1, 0.1)
SCALING_ERROR: PolarPoint = PolarPoint(10, 0.1)