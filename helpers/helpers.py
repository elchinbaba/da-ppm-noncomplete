from helpers.point_set.point_set import CartesianPointSet, PolarPointSet
from helpers.point_set.points import CartesianPoints, CartesianPoint, PolarPoint
from helpers.min_enc_circ import minimum_enclosing_circle
from helpers.min_enc_circ_welzl import welzl


def is_equal(a: float|PolarPoint, b: float|PolarPoint, err: float|PolarPoint) -> bool:
    if type(a) == PolarPoint and type(b) == PolarPoint and type(err) == PolarPoint:
        return is_equal(a.phi, b.phi, err.phi) and is_equal(a.r, b.r, err.r)

    try:
        return abs(a - b) < err
    except:
        raise